# -*- coding: utf-8 -*-
from arkindex_worker.worker import ElementsWorker


class Demo(ElementsWorker):
    def process_element(self, element):
        print("Demo processing element", element)


def main():
    Demo(description="Palm Leaf Manuscript Region Annotator").run()


if __name__ == "__main__":
    main()
