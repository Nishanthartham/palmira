# palmira

Palm Leaf Manuscript Region Annotator

### Development

For development and tests purpose it may be useful to install the worker as a editable package with pip.

```shell
pip3 install -e .
```

### Linter

Code syntax is analyzed before submitting the code.\
To run the linter tools suite you may use pre-commit.

```shell
pip install pre-commit
pre-commit run -a
```

### Run tests

Tests are executed with tox using [pytest](https://pytest.org).

```shell
pip install tox
tox
```

To recreate tox virtual environment (e.g. a dependencies update), you may run `tox -r`
