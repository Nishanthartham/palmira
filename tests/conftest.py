# -*- coding: utf-8 -*-
import os

import pytest
from arkindex_worker.worker.base import BaseWorker

from arkindex.mock import MockApiClient


@pytest.fixture(autouse=True)
def setup_environment(responses, monkeypatch):
    """Setup needed environment variables"""

    # Allow accessing remote API schemas
    # defaulting to the prod environment
    schema_url = os.environ.get(
        "ARKINDEX_API_SCHEMA_URL",
        "https://arkindex.teklia.com/api/v1/openapi/?format=openapi-json",
    )
    responses.add_passthru(schema_url)

    # Set schema url in environment
    os.environ["ARKINDEX_API_SCHEMA_URL"] = schema_url
    # Setup a fake worker run ID
    os.environ["ARKINDEX_WORKER_RUN_ID"] = "1234-{{ cookiecutter.slug }}"

    # Setup a mock api client instead of using a real one
    monkeypatch.setattr(BaseWorker, "setup_api_client", lambda _: MockApiClient())
