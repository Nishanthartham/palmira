FROM python:3

WORKDIR /src

# Install worker as a package
COPY worker_{{cookiecutter.slug}} worker_{{cookiecutter.slug}}
COPY requirements.txt setup.py VERSION ./
RUN pip install .

# Add archi local CA
RUN curl https://assets.teklia.com/teklia_dev_ca.pem > /usr/local/share/ca-certificates/arkindex-dev.crt && update-ca-certificates
ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/ca-certificates.crt

CMD ["worker-{{ cookiecutter.slug }}"]
